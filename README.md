```shell
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
pip install python-openstackclient
```

```shell
vagrant up 
# vagrant up --no-provision
vagrant ssh
sudo -u stack -i
git clone https://opendev.org/openstack/devstack
cd devstack
git checkout stable/xena
touch local.info
cp samples/local.conf local.conf
vim local.conf
```

```
[[local|localrc]]
ADMIN_PASSWORD=changeme
DATABASE_PASSWORD=$ADMIN_PASSWORD
RABBIT_PASSWORD=$ADMIN_PASSWORD
SERVICE_PASSWORD=$ADMIN_PASSWORD

HOST_IP=192.168.33.123
IPV4_ADDRS_SAFE_TO_USE=172.31.1.0/24
FLOATING_RANGE=192.168.20.0/25
MYSQL_HOST=192.168.33.123
SERVICE_HOST=192.168.33.123

DEST=/opt/stack
```

```shell
FORCE=yes ./stack.sh
```

Wait for 30 minutes ...

```shell
exit
exit
```

```shell
terraform init \
    -backend-config="address=https://gitlab.hzdr.de/api/v4/projects/<project-id>/terraform/state/<state-name>" \
    -backend-config="lock_address=https://gitlab.hzdr.de/api/v4/projects/<project-id>/terraform/state/<state-name>/lock" \
    -backend-config="unlock_address=https://gitlab.hzdr.de/api/v4/projects/<project-id>/terraform/state/<state-name>/lock" \
    -backend-config="username=<username>" \
    -backend-config="password=<project-personal-access-token>" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

```shell
terraform plan
terraform apply
# download <user>-openrc.sh from DevStack
source <user>-openrc.sh
openstack server list
vagrant ssh
ssh cirros@<public-ip>
# gocubsgo
exit
exit
terraform destroy
vagrant halt
#vagrant destroy -f
```
