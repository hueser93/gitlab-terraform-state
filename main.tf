provider "openstack" {
  user_name   = var.user_name
  tenant_name = var.tenant_name
  password    = var.application_access_data
  auth_url    = var.auth_url
}

resource "openstack_compute_keypair_v2" "keypair_terraform" {
  name = "keypair_terraform"
  public_key = var.personal_public_key
}

# Create a new network.
resource "openstack_networking_network_v2" "network_terraform" {
  name           = "network_terraform"
  admin_state_up = "true"
}

# Create a new subnet.
resource "openstack_networking_subnet_v2" "subnet_terraform" {
  name       = "subnet_terraform"
  network_id = openstack_networking_network_v2.network_terraform.id
  cidr       = var.network_subnet
  ip_version = 4
}

# Create a new security group with SSH port open.
resource "openstack_compute_secgroup_v2" "secgroup_terraform" {
  name        = "secgroup_terraform"
  description = "A security group for SSH connections."

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}

# Create a new port/network interface for the network with a fixed IP address.
resource "openstack_networking_port_v2" "port_terraform" {
  name               = "port_terraform"
  network_id         = openstack_networking_network_v2.network_terraform.id
  admin_state_up     = "true"
  security_group_ids = [ openstack_compute_secgroup_v2.secgroup_terraform.id ]

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.subnet_terraform.id
    ip_address = var.network_address
  }
}

# Create a new router that connects the network to the outside world.
# Enter the ID of your public network!
resource "openstack_networking_router_v2" "router_terraform" {
  name                = "router_terraform"
  external_network_id = var.public_network_id
}

# Create a new interface for the router.
resource "openstack_networking_router_interface_v2" "router_interface_terraform" {
  router_id = openstack_networking_router_v2.router_terraform.id
  subnet_id = openstack_networking_subnet_v2.subnet_terraform.id
}

# Create a new compute instance based on a chosen image.
resource "openstack_compute_instance_v2" "instance_terraform" {
  name            = "instance_terraform"
  security_groups = [ openstack_compute_secgroup_v2.secgroup_terraform.name ]
  image_name      = var.image_name
  flavor_name     = "m1.small"
  key_pair        = openstack_compute_keypair_v2.keypair_terraform.name

  network {
    port = openstack_networking_port_v2.port_terraform.id
  }
}

# Create a new floating IP.
resource "openstack_networking_floatingip_v2" "floating_ip_terraform" {
  pool = "public"
}

# Assign the floating IP to a port/network interface of the compute instance.
resource "openstack_networking_floatingip_associate_v2" "assoc_floating_ip_terraform" {
  floating_ip = openstack_networking_floatingip_v2.floating_ip_terraform.address
  port_id     = openstack_networking_port_v2.port_terraform.id
}
